set nocompatible

call pathogen#runtime_append_all_bundles()
call pathogen#helptags()

let mapleader = ","

noremap <leader>o <Esc>:CommandT<CR>
noremap <leader>0 <Esc>:CommandTFlush<CR>
noremap <leader>m <Esc>:CommandTBuffer<CR>

autocmd FileType html,htmldjango,jinjahtml,eruby,mako let b:closetag_html_style=1
autocmd FileType html,xhtml,xml,htmldjango,jinjahtml,eruby,mako source ~/.vim/bundle/closetag/plugin/closetag.vim

let g:tagbar_usearrows = 1
nnoremap <leader>l :TagbarToggle<CR>

set background=light
"let g:solarized_termtrans=1 
"let g:solarized_termcolors=256
"let g:solarized_contrast="high"
"let g:solarized_visibility="high"
"colorscheme solarized

set encoding=utf-8
let g:Powerline_symbols = 'fancy'
set laststatus=2
set fillchars+=stl:\ ,stlnc:\

syntax on
filetype on
filetype plugin indent on
set pastetoggle=<F3>
colorscheme Tomorrow-Night-Bright
set nolist
set showcmd
set mouse=a
set wrap
set formatoptions+=l
set lbr
set noerrorbells
set ofu=syntaxcomplete

set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab

set autoindent
set number
set modeline
set hidden
set backspace=indent,eol,start
set t_Co=256
set nobackup
set nowritebackup
set noswapfile
set textwidth=79
set wildmenu
set wildignore=*.pyc,venv*
set ruler
set ignorecase
set smartcase
set hlsearch
set incsearch

if has("gui_running")
    set guitablabel=%-0.12t%M
    set guioptions-=r
    set guioptions-=L
    set guioptions+=a
    " set gfn=Meslo\ LG\ L:h12
    " set gfn=Espresso\ Mono\ Regular:h12
    " colo tutticolori
    colo Tomorrow-Night-Bright
    "colo rdark
    set listchars=tab:▸\ ,eol:¬         " Invisibles using the Textmate style
endif

map <D-n> :!open -a /usr/bin/nautilus '.' <CR><CR>
map <leader><space> :nohl<CR>

autocmd bufwritepost .vimrc source ~/.vimrc

imap ;; <Esc>

nnoremap <leader>v <Esc>:e ~/.vimrc<CR>
nnoremap <leader>w <Esc>:w<CR>
nnoremap <leader>d <Esc>:q!<CR>
map <C-l> :tabn<CR>
map <C-h> :tabp<CR>
map <C-n> :tabnew<CR>
map <C-d> :tabclose<CR>
nnoremap <tab> %
vnoremap <tab> %

map <leader>n :NERDTreeToggle<CR>

let g:fullscreen = 0

function! ToggleFullscreen()
    if g:fullscreen == 1
        let g:fullscreen = 0
        let mod = "remove"
    else
        let g:fullscreen = 1
        let mod = "add"
    endif
    call system("wmctrl -ir " . v:windowid . " -b " . mod . ",fullscreen")
endfunction

map <silent> <F11> :call ToggleFullscreen()<CR>

function! Fillit()

" We start the python code like the next line.

python << EOF
import vim
current_line = vim.current.buffer[-1]
field_name = current_line.split('=')[0]
vim.current.buffer[-1] += '\n\n'
vim.current.buffer.append('property')
EOF
" Here the python code is closed. We can continue writing VimL or python again.
endfunction
map <silent> <F5> :call Fillit()<CR>
set clipboard=unnamed
